<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Affichage de reservation pour une chambre</h2>
	</header>
	<form action="" method="POST" >
		<div class="form-row">
			<div class="form-group col-md-3">
				<label for="idChambre">Entrer le num�ro de chambre :</label>
				<input type="number" name="idChambre" class="form-control">
			</div>
		</div>
		<input type="submit" class="btn btn-primary" value="Valider">
	</form>
	<a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>

</body>
</html>