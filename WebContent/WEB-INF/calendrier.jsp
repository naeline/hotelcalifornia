<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Calendrier de réservation</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/styleCalendrier.css">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<nav class="navbar navbar-dark bg-dark">
		<a class="navbar-brand" href="${pageContext.request.contextPath }/ServletResa">Reservation</a>
        <a class="navbar-brand" href="${pageContext.request.contextPath }/ServletClient">Client</a>
        <a class="navbar-brand" href="${pageContext.request.contextPath }/ServletAffichageChambre">Chambre</a>
    </nav>
	<header>
        <h1>HOTEL CALIFORNIA</h1>
        <h2>Planning de réservation</h2>
    </header>
    	
    <section>
        <div id="calendar" class="calendar"></div>
    </section>
	<footer>
		<p>Made by ISPIR Ozge and MALA Mouloud</p>
	</footer>
    <script type="text/javascript" src="js/appCalendrier.js"></script>
</body>
</html>