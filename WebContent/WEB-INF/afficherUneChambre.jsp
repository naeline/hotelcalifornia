<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Affichage d'une chambre</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Chambre</h2>
	</header>
	<p><c:out value="${maChambre.nom}"></c:out></p>
	<p><c:out value="${maChambre.nombreLit}"></c:out></p>
	<a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
</body>
</html>