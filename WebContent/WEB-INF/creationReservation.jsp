<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0 shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/css/Aristo/aristoResa.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/css/styleResa.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
	<title>Creation de Reservation</title>
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Veuillez entrer une réservation</h2>
	</header>
    <form id="resa" method="POST" action="">
		
		<p>Renseignez votre réservation : </p>
		<section class ="form-row">
			<input type="number" name="numeroClient" class ="form_control" placeholder="Numero Client">
			<input type="number" name="numeroChambre" class ="form_control" placeholder="Numero Chambre">
		</section>

		<section id="choixDate">
			<div class="blocDatepicker">
				<label for="arrivee">Date de début de reservation</label>
				<input type="text" class="datepicker" name="arrivee">
			</div>
			<div class="blocDatepicker">
				<label for="depart">Date de fin de reservation</label>
				<input type="text" class="datepicker2" name="depart">
			</div>
		</section>
		<input type="submit" value="Creer">
    </form>
    <a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
    
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="./js/appResa.js"></script>
</body>
</html>