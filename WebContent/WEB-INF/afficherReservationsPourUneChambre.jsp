<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Affichage resa Par jour</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Les réservations pour la chambre </h2>
	</header>

	<ul class="list-group">
	<c:forEach var="ligneParChambre" items="${ligneParChambre}">
		<li class="list-group-item list-group-item-danger"><c:out value="ARRIVEE : ${ligneParChambre.arrivee} - DEPART : ${ligneParChambre.depart}">Pas de reservation pour cette chambre</c:out></li>
	</c:forEach>
	</ul>
	<a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
</body>
</html>