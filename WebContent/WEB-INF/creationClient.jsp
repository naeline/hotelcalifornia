<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Creation client</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Cr�ation Nouveau Client</h2>
	</header>
	<form action="${pageContext.request.contextPath }/ServletCreationClient" method="POST">
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="nom">Entrer le nom</label>
				<input type="text" name="nom" class="form-control">
			</div>
			<div class="form-group col-md-6">
				<label for="prenom">Entrer le prenom</label>
				<input type="text" name="prenom" class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label for="adresse">Entrer l'adresse</label>
			<input type="text" name="adresse" class="form-control" id="inputAddress">
		</div>
		<div class="form-group">			
			<label for="telephone">Entrer le telephone</label>
			<input type="tel" name="telephone" class="form-control">	
		</div>
		<input type="submit" class="btn btn-primary">
	</form>
	<a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
</body>
</html>