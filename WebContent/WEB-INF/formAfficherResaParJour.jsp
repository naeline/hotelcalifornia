<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Choisir la date pour l'affichage</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/Aristo/aristoResa.css">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/styleResa.css">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Reservation par jour</h2>
	</header>
	<form action="" method="POST" >
		<div>
			<label for="jour">Entrez la date: </label>
			<input type="text" class="datepicker" name="jour">
		</div>
		<input type="submit" class="btn btn-primary" value="Valider">
	</form>
	<a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
	
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="./js/appResa.js"></script>
</body>
</html>