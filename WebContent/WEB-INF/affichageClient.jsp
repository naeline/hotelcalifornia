<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Fiche Client</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Fiches Client</h2>
	</header>
	<table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Numero Client</th>
            <th scope="col">Nom</th>
            <th scope="col">Prenom</th>
            <th scope="col">Adresse</th>
            <th scope="col">Telephone</th>
          </tr>
        </thead>
		<tbody>
			<c:forEach var="clients" items="${clients}">
				<tr>
					<th scope="row"><c:out value="${clients.idClient }">Pas de client</c:out></th>
					<td><c:out value="${clients.nom }">Pas de client</c:out></td>
					<td><c:out value="${clients.prenom }">Pas de client</c:out></td>
					<td><c:out value="${clients.adresse }">Pas de client</c:out></td>
					<td><c:out value="${clients.telephone }">Pas de client</c:out></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<a class="btn btn-primary" role="button" href="${pageContext.request.contextPath }/ServletCreationReservatoin">Entrer une nouvelle reservation</a>
	<a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
</body>
</html>