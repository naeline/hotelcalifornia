<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Les Reservations</h2>
	</header>
	<ul class="list-group">
	<c:forEach var="ReservationParJour" items="${ReservationParJour}">
		<li class="list-group-item list-group-item-danger">
		<c:out value="La chambre n�${ReservationParJour.chambre.toStringNumeroChambre()} ${lrm.chambre.toStringNomChambre()} 
		 est occup�e">
		 <c:set var="chambresOccupes" value="${chambresOccupes + 1}"/>
		</c:out>
		</li>
	</c:forEach>
	</ul>
	<li class="list-group-item list-group-item-primary"><c:out value="Il reste ${compteur} chambre(s) libre(s)"></c:out></li>
	<a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
</body>
</html>