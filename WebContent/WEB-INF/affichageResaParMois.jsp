<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Reservation du mois</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/styleCalendrier.css">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
        <h1>HOTEL CALIFORNIA</h1>
        <h2>Planning de r�servation</h2>
    </header>
	<form method="POST" action="">
		<div class="form-group">
    	<label for="mois">Selectionner le Mois :</label>
		    <select name="mois" class="form-control" id="exampleFormControlSelect1">
			    <option value="1">Janvier</option>
			    <option value="2">F�vrier</option>
			    <option value="3">Mars</option>
			    <option value="4">Avril</option>
			    <option value="5">Mai</option>
			    <option value="6">Juin</option>
			    <option value="7">Juillet</option>
			    <option value="8">Aout</option>
			    <option value="9">Septembre</option>
			    <option value="10">Octobre</option>
			    <option value="11">Novembre</option>
			    <option value="12">D�cembre</option>
			</select>
		</div>
		<ul class="list-group">
	    <c:forEach var="lrm" items="${listeResaMois }" varStatus="status">
			<li class="list-group-item list-group-item-danger">Resa : <c:out value="la chambre N�${lrm.chambre.toStringNumeroChambre()} ${lrm.chambre.toStringNomChambre()} est r�serv�e du ${lrm.arrivee} au ${lrm.depart}">Pas de reservation pour le mois s�lectionn�</c:out></li>
		</c:forEach>
		</ul>
		<input type="submit" class="btn btn-primary" name="ajoutListe">
    </form>
    
    <a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
</body>
</html>