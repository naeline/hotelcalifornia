<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Reservations</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Client</h2>
		<a class="btn btn-outline-dark" role="button" href="${pageContext.request.contextPath }/ServletAfficherReservationsPourUneChambre">Reservation par chambre</a>
		<a class="btn btn-secondary" role="button" href="${pageContext.request.contextPath }/ServletAffichageParJour">Reservation par jour</a>
		<a class="btn btn-dark" role="button" href="${pageContext.request.contextPath }/ServletAffichageResaParMois">Reservartion par mois</a>
		<a class="btn btn-primary" role="button" href="${pageContext.request.contextPath }/ServletCreationReservation">Entrer une nouvelle réservation</a>
	</header>
	<a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
</body>
</html>