<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>La liste des chambres</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Les chambres</h2>
	</header>
	<div class="row">	
		<c:forEach var="lesChambres" items="${chambres}">
			<div class="col-sm-4">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title"><c:out value="${lesChambres.nom}"></c:out></h5>
						<p class="card-text"><c:out value="Chambre au prix de ${lesChambres.prix}$ la nuit"></c:out></p>
					</div>
				</div>
			</div>
		</c:forEach> 
	</div>
	<a class="btn btn-info" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
</body>
</html>