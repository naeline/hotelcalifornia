<%@ include file="head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Client</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/style.css">
</head>
<body>
	<header>
		<h1>HOTEL CALIFORNIA</h1>
		<h2>Client</h2>
		<a class="btn btn-light" role="button" href="${pageContext.request.contextPath }/ServletAfficherTousLesClients">Afficher client</a>
		<a class="btn btn-secondary" role="button" href="${pageContext.request.contextPath }/ServletCreationClient">Entrer un nouveau client</a>
		<a class="btn btn-dark" role="button" href="${pageContext.request.contextPath }/ServletCalendrier">Accueil</a>
	</header>
</body>
</html>