package com.lou.hotelCalifornia.dal;

import java.util.List;

import com.lou.hotelCalifornia.bo.Client;

public interface ClientDao {
	
	//j'ai supprim� le delete

	public Client insert(Client client) throws Exception;
	
	public Client update(Client client) throws Exception;
	
	
	/*TODO
	 * a voir pour affichage de fiche client
	 */
	
	public List<Client> selectAll() throws Exception;
}
