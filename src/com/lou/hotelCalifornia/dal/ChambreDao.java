package com.lou.hotelCalifornia.dal;

import java.util.List;

import com.lou.hotelCalifornia.bo.Chambre;

public interface ChambreDao {

	public Chambre update(Chambre chambre) throws Exception;
	
	public List<Chambre> selectAll() throws Exception;

	public Chambre selectById(int idChambre) throws Exception;
	
	//public boolean delete(Chambre chambre) throws Exception;
}
