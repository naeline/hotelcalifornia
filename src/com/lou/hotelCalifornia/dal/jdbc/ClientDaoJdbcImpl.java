package com.lou.hotelCalifornia.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.lou.hotelCalifornia.bo.Client;
import com.lou.hotelCalifornia.dal.ClientDao;

import com.lou.hotelCalifornia.dal.jdbc.MySQLConnection;

public class ClientDaoJdbcImpl implements ClientDao {
	
	private final static String SELECT_ALL = "SELECT * FROM Clients";

	private final static String INSERT = "INSERT INTO Clients(nom, prenom, adresse, telephone) VALUES(?,?,?,?)";
	
	//j'enl�ve le DELETE ET LA FONCTION delete
	
	
	
	
	@Override
	public Client insert(Client client) throws Exception {
		
				if(client == null) {
					throw new Exception("Pas de client");
				}
				
				try(Connection cnx = MySQLConnection.getConnection()) {
					
					PreparedStatement pStmt = cnx.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
					pStmt.setString(1, client.getNom());
					pStmt.setString(2, client.getPrenom());
					pStmt.setString(3, client.getAdresse());
					pStmt.setString(4, client.getTelephone());
					
					int n = pStmt.executeUpdate();
					
					ResultSet rs = pStmt.getGeneratedKeys();
					if(n > 0 && rs.next()) {
						int id = (int) rs.getLong(1);
						client.setIdClient(id);
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				return client;
	}

	@Override
	public Client update(Client client) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	

	@Override
	public List<Client> selectAll() throws Exception {
		
		List<Client> clients = new ArrayList<Client>();
		
		try(Connection cnx = MySQLConnection.getConnection()) {

			Statement stmt = cnx.createStatement();
			
			ResultSet rs = stmt.executeQuery(SELECT_ALL);
			
			while(rs.next()) {
				clients.add(map(rs));
			}					
		} catch (Exception e) {
			e.printStackTrace();
		}				
		return clients;
	}

	private Client map(ResultSet rs) throws SQLException {
		int id = rs.getInt("idClient");
		String nom = rs.getString("nom");
		String prenom = rs.getString("prenom");
		String adresse = rs.getString("adresse");
		String telephone = rs.getString("telephone");
		return new Client(id, nom, prenom, adresse, telephone);
	}

}
