package com.lou.hotelCalifornia.dal.jdbc;



import com.lou.hotelCalifornia.bo.Chambre;
import com.lou.hotelCalifornia.bo.Client;
import com.lou.hotelCalifornia.dal.ChambreDao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ChambreDaoJdbcImpl implements ChambreDao {

	private final static String SELECT_ALL = "SELECT * FROM chambres";
	
	private final static String SELECT_BY_ID = "SELECT * FROM chambres WHERE idChambre=? ";
	
	@Override
	public Chambre update(Chambre chambre) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Chambre> selectAll() throws Exception {
		
		List<Chambre> chambres = new ArrayList<Chambre>();
		
		try(Connection cnx = MySQLConnection.getConnection()) {

			Statement stmt = cnx.createStatement();
			
			ResultSet rs = stmt.executeQuery(SELECT_ALL);
			
			while(rs.next()) {
				chambres.add(map(rs));
			}					
		} catch (Exception e) {
			e.printStackTrace();
		}				
		return chambres;
	}
	
	
	public Chambre selectById(int idChambre) throws Exception{
		
		Chambre chambre = null;
;		
		try(Connection cnx = MySQLConnection.getConnection()) {

			PreparedStatement pStmt = cnx.prepareStatement(SELECT_BY_ID);
			pStmt.setInt(1, idChambre);
			ResultSet rs = pStmt.executeQuery();
			
			while(rs.next()) {
				if (chambre == null) {
					chambre = map(rs);
				}
			}					
		} catch (Exception e) {
			e.printStackTrace();
		}				
		
		return chambre;
	}

	public static Chambre map(ResultSet rs) throws SQLException{
		
		int idChambre = rs.getInt("idChambre");
		String nomChambre = rs.getString("nom");
		int nombreLit = rs.getInt("nombreLit");
		float prix = rs.getFloat("prix");;
		return new Chambre(idChambre, nomChambre, nombreLit, prix);
	}

}
