package com.lou.hotelCalifornia.dal.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import com.lou.hotelCalifornia.bo.Chambre;
import com.lou.hotelCalifornia.bo.LigneReservation;
import com.lou.hotelCalifornia.bo.Reservation;
import com.lou.hotelCalifornia.dal.LigneReservationDao;

public class LigneReservationDaoJdbcImpl implements LigneReservationDao{

	private final static String SELECT_BY_ID = "SELECT * FROM hotel.lignesreservation " + 
			"JOIN hotel.chambres ON idchambre=chambre " + 
			"JOIN hotel.reservations ON idReservation=reservation " + 
			"WHERE chambre=?;";
	
	/*
	 * IL Y A UN PROBLEME DE DATE A REGLER
	 */
	private final static String SELECT_BY_JOUR= "SELECT * FROM hotel.lignesreservation " + 
			"JOIN hotel.chambres ON idchambre=chambre " + 
			"JOIN hotel.reservations ON idReservation=reservation " + 
			"WHERE (arrivee<=? AND ?<depart);";
	
	private final static String SELECT_BY_MONTH = "SELECT * FROM LignesReservation JOIN chambres ON idChambre = chambre JOIN reservations ON idReservation=reservation WHERE MONTH(arrivee) = ? OR MONTH(depart) = ? ORDER BY arrivee";

	
	public List<LigneReservation> selectByChambre(int chambre) throws Exception {
			
			List<LigneReservation> ligneParChambre = new ArrayList<LigneReservation>();
			
			try(Connection cnx = MySQLConnection.getConnection()) {
				PreparedStatement pStmt = cnx.prepareStatement(SELECT_BY_ID);
				pStmt.setInt(1, chambre);
				ResultSet rs = pStmt.executeQuery();
				while(rs.next()) {
					ligneParChambre.add(map(rs));
				}
			} catch (Exception e) {
				e.printStackTrace();
				}
	
			return ligneParChambre;
		}

	private LigneReservation map(ResultSet rs) throws SQLException {
		Reservation reservation = ReservationDaoJdbcImpl.map(rs);
		int ligneReservation = rs.getInt("ligneReservation");
		Chambre chambre = ChambreDaoJdbcImpl.map(rs);
		LocalDate arrivee = rs.getDate("arrivee").toLocalDate();
		LocalDate depart = rs.getDate("depart").toLocalDate();
		return new LigneReservation(reservation, ligneReservation, chambre, arrivee, depart);
	}

	@Override
	public List<LigneReservation> selectBySemaine() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LigneReservation> selectByMois(int mois) throws Exception {
		
		List<LigneReservation> ligneParMois = new ArrayList<LigneReservation>();
			
		try(Connection cnx = MySQLConnection.getConnection()) {
			PreparedStatement pStmt = cnx.prepareStatement(SELECT_BY_MONTH);
			pStmt.setInt(1, mois);
			pStmt.setInt(2, mois);
			System.out.println(pStmt.toString());
			ResultSet rs = pStmt.executeQuery();
			while(rs.next()) {
				ligneParMois.add(map(rs));
			}
		} catch (Exception e) {
			e.printStackTrace();
			}

		return ligneParMois;
	}

	@Override
	public List<LigneReservation> selectByJour(LocalDate dateChoisie) throws Exception {
		List<LigneReservation> ReservationParJour = new ArrayList<LigneReservation>();
		
		try(Connection cnx = MySQLConnection.getConnection()) {
			PreparedStatement pStmt = cnx.prepareStatement(SELECT_BY_JOUR);
			pStmt.setDate(1, Date.valueOf(dateChoisie));
			pStmt.setDate(2,Date.valueOf(dateChoisie));
			System.out.println(pStmt.toString());
			ResultSet rs = pStmt.executeQuery();
			while(rs.next()) {
				ReservationParJour.add(map(rs));
			}
		} catch (Exception e) {
			e.printStackTrace();
			}

		return ReservationParJour;
	}
}
