package com.lou.hotelCalifornia.dal.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;

import com.lou.hotelCalifornia.bo.Reservation;
import com.lou.hotelCalifornia.dal.ReservationDao;

public class ReservationDaoJdbcImpl implements ReservationDao {
	
	private final static String INSERT = "INSERT INTO Reservations(client, le, payeeLe) VALUES(?,?,?)";
	
	private final static String INSERT_LIGNE_RESERVATION = "INSERT INTO lignesreservation(reservation, ligneReservation, chambre, arrivee, depart) VALUES(?,?,?,?,?)";
	
	
	@Override
	public Reservation insert(Reservation reservation) throws Exception {

		try(Connection cnx = MySQLConnection.getConnection()) {
			try {
				//d�sactive l'auto-commit
				cnx.setAutoCommit(false);
				
				PreparedStatement pStmt = cnx.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
				pStmt.setInt(1, reservation.getClient().getIdClient());
				pStmt.setDate(2, Date.valueOf(reservation.getLe()));
				pStmt.setDate(3, Date.valueOf(reservation.getPayeele()));
				
				pStmt.executeUpdate();
				
				ResultSet rs = pStmt.getGeneratedKeys();
				if(rs.next()) {
					int id = rs.getInt(1);
					reservation.setIdReservation(id);
				}
				
				pStmt = cnx.prepareStatement(INSERT_LIGNE_RESERVATION);
				
				for(int i = 0; i < reservation.getListeReservations().size(); i++) {
					pStmt.setInt(1, reservation.getIdReservation());
					pStmt.setInt(2, i+1);
					pStmt.setInt(3, reservation.getListeReservations().get(i).getChambre().getIdChambre());
					pStmt.setDate(4, Date.valueOf(reservation.getListeReservations().get(i).getArrivee()));
					pStmt.setDate(5, Date.valueOf(reservation.getListeReservations().get(i).getDepart()));
				}				
				pStmt.executeUpdate();
			
				cnx.commit();
			} catch (SQLException e) {
				e.printStackTrace();
				cnx.rollback();
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return reservation;
	}

	@Override
	public Reservation update(Reservation reservation) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean delete(Reservation reservation) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	public static Reservation map(ResultSet rs) {
		// TODO Auto-generated method stub
		return null;
	}

}
