package com.lou.hotelCalifornia.dal;

import java.util.List;

import com.lou.hotelCalifornia.bo.Reservation;


public interface ReservationDao {
	
	
	public Reservation insert(Reservation reservation) throws Exception;
	
	public Reservation update(Reservation reservation) throws Exception;
	
	public boolean delete(Reservation reservation) throws Exception;
}
