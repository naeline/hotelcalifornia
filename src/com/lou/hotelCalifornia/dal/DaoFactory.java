package com.lou.hotelCalifornia.dal;

import com.lou.hotelCalifornia.dal.jdbc.ChambreDaoJdbcImpl;
import com.lou.hotelCalifornia.dal.jdbc.ClientDaoJdbcImpl;
import com.lou.hotelCalifornia.dal.jdbc.LigneReservationDaoJdbcImpl;
import com.lou.hotelCalifornia.dal.jdbc.ReservationDaoJdbcImpl;

public class DaoFactory {

	public static ClientDao getClientDao() {
		return new ClientDaoJdbcImpl();
	}
	
	public static ReservationDao getReservationDao() {
		return new ReservationDaoJdbcImpl();
	}
	
	public static ChambreDao getChambreDao() {
		return new ChambreDaoJdbcImpl();
	}

	public static LigneReservationDao getLigneReservationDao() {
		return new LigneReservationDaoJdbcImpl();
	}
}
