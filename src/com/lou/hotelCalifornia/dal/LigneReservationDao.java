package com.lou.hotelCalifornia.dal;

import java.time.LocalDate;
import java.util.List;

import com.lou.hotelCalifornia.bo.LigneReservation;
import com.lou.hotelCalifornia.bo.Reservation;

public interface LigneReservationDao {
	
	
	public List<LigneReservation> selectByChambre(int idChambre) throws Exception;
	
	public List<LigneReservation> selectByJour(LocalDate dateChoisie) throws Exception;
	
	public List<LigneReservation> selectBySemaine() throws Exception;
	
	public List<LigneReservation> selectByMois(int mois) throws Exception;

}
