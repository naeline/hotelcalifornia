package com.lou.hotelCalifornia.bo;


public class Chambre {

	private int idChambre;
	
	private String nom;
	
	private int nombreLit;
	
	private float prix;

	public int getIdChambre() {
		return idChambre;
	}

	public void setIdChambre(int idChambre) {
		this.idChambre = idChambre;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNombreLit() {
		return nombreLit;
	}

	public void setNombreLit(int nombreLit) {
		this.nombreLit = nombreLit;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}
	
	

	public Chambre() {
		super();
	}

	
	public Chambre(int idChambre) {
		super();
		this.idChambre = idChambre;
	}

	public Chambre(String nom, int nombreLit, float prix) {
		super();
		this.nom = nom;
		this.nombreLit = nombreLit;
		this.prix = prix;
	}

	public Chambre(int idChambre, String nom, int nombreLit, float prix) {
		super();
		this.idChambre = idChambre;
		this.nom = nom;
		this.nombreLit = nombreLit;
		this.prix = prix;
	}
	
	
	public String toStringNumeroChambre() {
		StringBuilder builder = new StringBuilder();
		builder.append(idChambre);
		return builder.toString();
	}
	
	public String toStringNomChambre() {
		StringBuilder builder = new StringBuilder();
		builder.append(nom);
		return builder.toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Chambre [idChambre=");
		builder.append(idChambre);
		builder.append(", nom=");
		builder.append(nom);
		builder.append(", nombreLit=");
		builder.append(nombreLit);
		builder.append(", prix=");
		builder.append(prix);
		builder.append("]");
		return builder.toString();
	}
	
}
