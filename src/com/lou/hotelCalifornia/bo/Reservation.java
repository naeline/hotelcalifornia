package com.lou.hotelCalifornia.bo;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Reservation {

	private int idReservation;
	
	private Client client;
	
	private LocalDate le;
	
	private LocalDate payeele;
	
	private List<LigneReservation> listeReservations = new ArrayList<LigneReservation>();
	

	public int getIdReservation() {
		return idReservation;
	}

	public void setIdReservation(int idReservation) {
		this.idReservation = idReservation;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public LocalDate getLe() {
		return le;
	}

	public void setLe(LocalDate le) {
		this.le = le;
	}

	public LocalDate getPayeele() {
		return payeele;
	}

	public void setPayeele(LocalDate payeele) {
		this.payeele = payeele;
	}

	public List<LigneReservation> getListeReservations() {
		return listeReservations;
	}

	public void setListeReservations(List<LigneReservation> listeReservations) {
		this.listeReservations = listeReservations;
	}
	
	public Reservation() {
		super();
	}

	public Reservation(Client client, LocalDate le, LocalDate payeele) {
		super();
		this.client = client;
		this.le = le;
		this.payeele = payeele;
	}

	public Reservation(int idReservation, Client client, LocalDate le, LocalDate payeele,
			List<LigneReservation> listeReservations) {
		super();
		this.idReservation = idReservation;
		this.client = client;
		this.le = le;
		this.payeele = payeele;
		this.listeReservations = listeReservations;
	}

	

	/*TODO
	 * Penser � boucler pour la liste de resa soit ici soit en jstl
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Reservation [idReservation=");
		builder.append(idReservation);
		builder.append(", client=");
		builder.append(client);
		builder.append(", le=");
		builder.append(le);
		builder.append(", payeele=");
		builder.append(payeele);
		builder.append(", listeReservations=");
		builder.append(listeReservations);
		builder.append("]");
		return builder.toString();
	}

	public void ajouterLigne(LigneReservation ligneReservation) {
		this.listeReservations.add(ligneReservation);
		
	}	
}
