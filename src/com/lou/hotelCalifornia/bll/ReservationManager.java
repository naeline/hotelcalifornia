package com.lou.hotelCalifornia.bll;

import java.time.LocalDate;
import java.time.ZoneId;

import com.lou.hotelCalifornia.bo.Chambre;
import com.lou.hotelCalifornia.bo.Client;
import com.lou.hotelCalifornia.bo.LigneReservation;
import com.lou.hotelCalifornia.bo.Reservation;
import com.lou.hotelCalifornia.dal.DaoFactory;

public class ReservationManager {

	public Reservation insert(int idClient, int idChambre, LocalDate arrivee, LocalDate depart) throws Exception {
		
		
		LocalDate aujourdhui = LocalDate.now();
		
		Client client = new Client(idClient);
		Chambre chambre = new Chambre(idChambre);
		
		Reservation reservation = new Reservation(client, aujourdhui, aujourdhui);
		reservation.ajouterLigne(new LigneReservation(reservation, chambre, arrivee, depart));
		
		return DaoFactory.getReservationDao().insert(reservation);
	}
}
