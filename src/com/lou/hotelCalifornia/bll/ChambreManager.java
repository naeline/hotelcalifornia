package com.lou.hotelCalifornia.bll;

import java.util.List;

import com.lou.hotelCalifornia.bo.Chambre;
import com.lou.hotelCalifornia.dal.DaoFactory;

public class ChambreManager {
	
	public List<Chambre> selectAll() throws Exception{
		return DaoFactory.getChambreDao().selectAll();
	}

	public Chambre selectById(int idChambre)throws Exception{
		return DaoFactory.getChambreDao().selectById(idChambre);
	}
	
}
