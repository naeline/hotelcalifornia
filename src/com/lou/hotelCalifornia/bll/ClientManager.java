package com.lou.hotelCalifornia.bll;

import java.util.List;

import com.lou.hotelCalifornia.bo.Client;
import com.lou.hotelCalifornia.dal.DaoFactory;

public class ClientManager {

	public Client insert(String nom, String prenom, String adresse, String telephone) throws Exception {
		
		Client client = new Client(nom, prenom, adresse, telephone);
		return DaoFactory.getClientDao().insert(client);
	}
	
	public List<Client> selectAll() throws Exception {
		return DaoFactory.getClientDao().selectAll();
	}
	
	
}
