package com.lou.hotelCalifornia.bll;

import java.time.LocalDate;
import java.util.List;

import com.lou.hotelCalifornia.bo.LigneReservation;
import com.lou.hotelCalifornia.dal.DaoFactory;

public class LigneReservationManager {

	public List<LigneReservation> selectByMois(int mois)throws Exception{
		return DaoFactory.getLigneReservationDao().selectByMois(mois);
	}
	
	public List<LigneReservation> selectByJour(LocalDate dateChoisie)throws Exception{
		return DaoFactory.getLigneReservationDao().selectByJour(dateChoisie);
	}
	
	public List<LigneReservation> selectByChambre(int idChambre)throws Exception{
		return DaoFactory.getLigneReservationDao().selectByChambre(idChambre);
	}
	
}
