package com.lou.hotelCalifornia.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lou.hotelCalifornia.bll.ReservationManager;
import com.lou.hotelCalifornia.bo.Reservation;


@WebServlet("/ServletCreationReservation")
public class ServletCreationReservation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public ServletCreationReservation() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/creationReservation.jsp");
		rd.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String saisieIdClient = request.getParameter("numeroClient");
		String saisieIdChambre = request.getParameter("numeroChambre");
		String saisieDateArrivee = request.getParameter("arrivee");
		String saisieDateDepart = request.getParameter("depart");
		
		int idClient = Integer.valueOf(saisieIdClient);
		int idChambre = Integer.valueOf(saisieIdChambre);
		LocalDate dateArrivee = LocalDate.parse(saisieDateArrivee);
		LocalDate dateDepart = LocalDate.parse(saisieDateDepart);
		
		System.out.println("DOPOST");
		System.out.println(idClient);
		System.out.println(idChambre);
		System.out.println(dateArrivee);
		System.out.println(dateDepart);
		
		HttpSession session = request.getSession();
		
		try {
			Reservation reservation = new ReservationManager().insert(idClient, idChambre, dateArrivee, dateDepart);
			request.setAttribute("listes", reservation);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/calendrier.jsp");
		rd.forward(request, response);
	}

}
