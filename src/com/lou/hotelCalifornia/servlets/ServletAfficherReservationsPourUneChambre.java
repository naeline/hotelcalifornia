package com.lou.hotelCalifornia.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lou.hotelCalifornia.bll.LigneReservationManager;
import com.lou.hotelCalifornia.bo.LigneReservation;



/**
 * MOULOUD N'A PAS CETTE PAGE
 */
/**
 * Servlet implementation class ServletAfficherReservationsPourUneChambre
 */
@WebServlet("/ServletAfficherReservationsPourUneChambre")
public class ServletAfficherReservationsPourUneChambre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ServletAfficherReservationsPourUneChambre() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/formAfficherResaPourUneChambre.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost");
		
		String idChambre_saisie = request.getParameter("idChambre");
		System.out.println(idChambre_saisie);
		int id = Integer.valueOf(idChambre_saisie);
		
		
		List<LigneReservation> ligneParChambre = new ArrayList<LigneReservation>();
		
		try {
			ligneParChambre = new LigneReservationManager().selectByChambre(id);
			request.setAttribute("ligneParChambre", ligneParChambre);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(ligneParChambre);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/afficherReservationsPourUneChambre.jsp");
		rd.forward(request, response);
	}

}
