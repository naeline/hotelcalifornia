package com.lou.hotelCalifornia.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lou.hotelCalifornia.bll.ChambreManager;
import com.lou.hotelCalifornia.bo.Chambre;

/**
 * Servlet implementation class ServletAffichageDUneChambre
 */
@WebServlet("/ServletAffichageDUneChambre")
public class ServletAffichageDUneChambre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAffichageDUneChambre() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/formulairePourAfficherUneChambre.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost");
		
		String idChambre_saisie = request.getParameter("idChambre");
		int id = Integer.valueOf(idChambre_saisie);
		Chambre maChambre = null;
		System.out.println(idChambre_saisie);
		try {
			maChambre = new ChambreManager().selectById(id);
			System.out.println(maChambre);
			request.setAttribute("maChambre", maChambre);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(maChambre);
		
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/afficherUneChambre.jsp");
		rd.forward(request, response);
	}

}
