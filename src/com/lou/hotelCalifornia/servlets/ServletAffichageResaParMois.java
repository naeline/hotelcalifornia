package com.lou.hotelCalifornia.servlets;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lou.hotelCalifornia.bll.LigneReservationManager;
import com.lou.hotelCalifornia.bo.LigneReservation;


@WebServlet("/ServletAffichageResaParMois")
public class ServletAffichageResaParMois extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public ServletAffichageResaParMois() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/affichageResaParMois.jsp");
		rd.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String moisSaisi = request.getParameter("mois");
		int moisNombre = Integer.valueOf(moisSaisi);
		System.out.println(moisSaisi);
		try {
			List<LigneReservation> listeResaMois = new LigneReservationManager().selectByMois(moisNombre);
			request.setAttribute("listeResaMois", listeResaMois);
			System.out.println(listeResaMois);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
//		Date now = new Date();
//		Calendar c = Calendar.getInstance();
//		c.setTime(now);
//		
//		int year = c.get(Calendar.YEAR);
//		int month = c.get(Calendar.MONTH);
//	    int day = c.get(Calendar.DAY_OF_MONTH);
//	    System.out.println(year);
		
		
		doGet(request, response);
	}

}
