package com.lou.hotelCalifornia.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

import com.lou.hotelCalifornia.bll.ClientManager;
import com.lou.hotelCalifornia.bo.Client;

/**
 * Servlet implementation class ServletAfficherTousLesClients
 */
@WebServlet("/ServletAfficherTousLesClients")
public class ServletAfficherTousLesClients extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public ServletAfficherTousLesClients() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Client> clients = new ArrayList<Client>();
		
		try {
			clients = new ClientManager().selectAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(clients);
		
		request.setAttribute("clients", clients);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/affichageClient.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
