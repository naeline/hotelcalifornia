package com.lou.hotelCalifornia.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lou.hotelCalifornia.bll.ClientManager;
import com.lou.hotelCalifornia.bo.Client;

/**TODO
 * A RECUP POUR MA PETITE OZGE
 * @author Aasus
 *
 */

@WebServlet("/ServletCreationClient")
public class ServletCreationClient extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ServletCreationClient() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/creationClient.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/*
		 * INSERT POUR AFFICHER CLIENT
		 */
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String adresse = request.getParameter("adresse");
		String telephone = request.getParameter("telephone");
		System.out.println("DOPOST");
		System.out.println(nom);
		
		try {
			Client client = new ClientManager().insert(nom, prenom, adresse, telephone);
			request.setAttribute("client", client);
			System.out.println(client);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			List<Client> listeClient = new ClientManager().selectAll();
			request.setAttribute("listes", listeClient);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("erreurs", e);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/ServletAfficherTousLesClients");
		rd.forward(request, response);
		
	}

}
