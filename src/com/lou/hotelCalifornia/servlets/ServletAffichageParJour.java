package com.lou.hotelCalifornia.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lou.hotelCalifornia.bll.LigneReservationManager;
import com.lou.hotelCalifornia.bo.LigneReservation;


@WebServlet("/ServletAffichageParJour")
public class ServletAffichageParJour extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public ServletAffichageParJour() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/formAfficherResaParJour.jsp");
		rd.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost");
		
		String jour_saisie = request.getParameter("jour");
		System.out.println(jour_saisie);
		LocalDate jour = LocalDate.parse(jour_saisie);
		System.out.println(jour);
		
		
		List<LigneReservation> ReservationParJour = new ArrayList<LigneReservation>();
		
		
		try {
			ReservationParJour = new LigneReservationManager().selectByJour(jour);
			request.setAttribute("ReservationParJour", ReservationParJour);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(ReservationParJour);
		int compteur = 6 - ReservationParJour.size();
		request.setAttribute("compteur", compteur);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/afficherResaParJour.jsp");
		rd.forward(request, response);
		
	}

}
