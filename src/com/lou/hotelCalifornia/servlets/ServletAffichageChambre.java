package com.lou.hotelCalifornia.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

import com.lou.hotelCalifornia.bll.ChambreManager;
import com.lou.hotelCalifornia.bo.Chambre;

/**
 * Servlet implementation class ServletAffichageChambre
 */
@WebServlet("/ServletAffichageChambre")
public class ServletAffichageChambre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ServletAffichageChambre() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Chambre> chambres = new ArrayList<Chambre>();
		
		try {
			chambres = new ChambreManager().selectAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(chambres);
		
		request.setAttribute("chambres", chambres);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/affichageChambre.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
